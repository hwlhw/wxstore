package com.ly.wxstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.SalerUpHierarchy;
import com.ly.wxstore.repository.SalerUpHierarchyDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class SalerUpHierarchyService {

	@Autowired
	private SalerUpHierarchyDao salerUpHierarchyDao;

	public SalerUpHierarchy getById(Long id) {
		return salerUpHierarchyDao.getById(id);
	}

	public List<SalerUpHierarchy> getAll() {
		return salerUpHierarchyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<SalerUpHierarchy> searchPage(SalerUpHierarchy salerUpHierarchy, int currentPage, int pageSize) {
		MyPage<SalerUpHierarchy> myPage = new MyPage<SalerUpHierarchy>();

		Long count = salerUpHierarchyDao.searchCount(salerUpHierarchy);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<SalerUpHierarchy> list = salerUpHierarchyDao.searchPage(salerUpHierarchy, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(SalerUpHierarchy salerUpHierarchy) {
		salerUpHierarchyDao.save(salerUpHierarchy);
	}

	public void update(SalerUpHierarchy salerUpHierarchy) {
		salerUpHierarchyDao.update(salerUpHierarchy);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		salerUpHierarchyDao.delete(id);
	}
}
