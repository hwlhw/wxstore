package com.ly.wxstore.repository.goods;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.goods.GoodsOrder;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface GoodsOrderDao {
	
	GoodsOrder getById(Long id);
	
	List<GoodsOrder> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<GoodsOrder> searchPage(@Param("goodsOrder")GoodsOrder goodsOrder,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(GoodsOrder goodsOrder);
	
	void save(GoodsOrder goodsOrder);
	
	void update(GoodsOrder goodsOrder);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	List<GoodsOrder> getByOpenid(String openid);
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 更新订单支付状态 
	 * @param outTradeNo 订单编号
	 * @param payStatus 订单支付状态
	 * @return
	 */
	void updatePayStatus(@Param("outTradeNo")String outTradeNo, @Param("payStatus")Long payStatus);
	
	/**
	 * 更新订单状态
	 * @param outTradeNo 订单编号
	 * @param orderStatus 订单状态
	 * @return
	 */
	void updateOrderStatus(@Param("outTradeNo")String outTradeNo, @Param("orderStatus")Long orderStatus);
	
	/**
	 * 更新订单状态和支付状态
	 * @param outTradeNo 订单编号
	 * @param orderStatus 订单状态
	 * @return
	 */
	void updateOrderStatusAndPayStatus(@Param("outTradeNo")String outTradeNo, @Param("orderStatus")Long orderStatus, @Param("payStatus")Long payStatus);

	/**
	 * 根据订单编号查询订单及其详情
	 * @param orderCode 订单编号
	 * @return GoodsOrder 订单及其详情
	 */
	GoodsOrder getByOrderCode(String orderCode);

}
