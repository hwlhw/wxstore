package com.ly.wxstore.service.goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.GoodsDeliveryAddress;
import com.ly.wxstore.repository.goods.GoodsDeliveryAddressDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsDeliveryAddressService {

	@Autowired
	private GoodsDeliveryAddressDao goodsDeliveryAddressDao;

	public GoodsDeliveryAddress getById(Long id) {
		return goodsDeliveryAddressDao.getById(id);
	}

	public List<GoodsDeliveryAddress> getAll() {
		return goodsDeliveryAddressDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsDeliveryAddress> searchPage(GoodsDeliveryAddress goodsDeliveryAddress, int currentPage, int pageSize) {
		MyPage<GoodsDeliveryAddress> myPage = new MyPage<GoodsDeliveryAddress>();

		Long count = goodsDeliveryAddressDao.searchCount(goodsDeliveryAddress);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsDeliveryAddress> list = goodsDeliveryAddressDao.searchPage(goodsDeliveryAddress, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsDeliveryAddress goodsDeliveryAddress) {
		goodsDeliveryAddressDao.save(goodsDeliveryAddress);
	}

	public void update(GoodsDeliveryAddress goodsDeliveryAddress) {
		goodsDeliveryAddressDao.update(goodsDeliveryAddress);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsDeliveryAddressDao.delete(id);
	}

}
