package com.ly.wxstore.rest.dto.news;

public class News_item {

	private String title;

	private String author;

	private String digest;

	private String content;

	private String content_source_url;

	private String thumb_media_id;

	private String show_cover_pic;

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getDigest() {
		return this.digest;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}

	public String getContent_source_url() {
		return this.content_source_url;
	}

	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}

	public String getThumb_media_id() {
		return this.thumb_media_id;
	}

	public void setShow_cover_pic(String show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}

	public String getShow_cover_pic() {
		return this.show_cover_pic;
	}
}
