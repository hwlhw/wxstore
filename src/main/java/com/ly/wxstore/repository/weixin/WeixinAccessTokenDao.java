package com.ly.wxstore.repository.weixin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.weixin.WeixinAccessToken;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinAccessTokenDao {
	
	WeixinAccessToken getById(Long id);
	
	List<WeixinAccessToken> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinAccessToken> searchPage(@Param("weixinAccessToken")WeixinAccessToken weixinAccessToken,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinAccessToken weixinAccessToken);
	
	void save(WeixinAccessToken weixinAccessToken);
	
	void update(WeixinAccessToken weixinAccessToken);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	WeixinAccessToken getByWeixinPublicId(Long publicId);
	

}
