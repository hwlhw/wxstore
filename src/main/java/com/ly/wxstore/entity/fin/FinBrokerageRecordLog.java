package com.ly.wxstore.entity.fin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class FinBrokerageRecordLog {

	public FinBrokerageRecordLog() {
	}
	
	private Long id; //
	private String outSid; //下单人sid
	private String inSid; //受益人sid
	private Long level; //销售级别(下单人的上线级数)
	private String orderCode; //订单编号
	private Double orderMoney; //订单金额
	private Double brokerage; //佣金
	private Long brokerageStatus; //佣金状态
	private Date createDate; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *下单人sid
     **/
	public String getOutSid(){
		return outSid;
	}
	
	/**
	 *下单人sid
	 **/
	public void setOutSid(String outSid){
		this.outSid=outSid;
	}
    /**
     *受益人sid
     **/
	public String getInSid(){
		return inSid;
	}
	
	/**
	 *受益人sid
	 **/
	public void setInSid(String inSid){
		this.inSid=inSid;
	}
    /**
     *销售级别(下单人的上线级数)
     **/
	public Long getLevel(){
		return level;
	}
	
	/**
	 *销售级别(下单人的上线级数)
	 **/
	public void setLevel(Long level){
		this.level=level;
	}
    /**
     *订单编号
     **/
	public String getOrderCode(){
		return orderCode;
	}
	
	/**
	 *订单编号
	 **/
	public void setOrderCode(String orderCode){
		this.orderCode=orderCode;
	}
    /**
     *订单金额
     **/
	public Double getOrderMoney(){
		return orderMoney;
	}
	
	/**
	 *订单金额
	 **/
	public void setOrderMoney(Double orderMoney){
		this.orderMoney=orderMoney;
	}
    /**
     *佣金
     **/
	public Double getBrokerage(){
		return brokerage;
	}
	
	/**
	 *佣金
	 **/
	public void setBrokerage(Double brokerage){
		this.brokerage=brokerage;
	}
    /**
     *佣金状态
     **/
	public Long getBrokerageStatus(){
		return brokerageStatus;
	}
	
	/**
	 *佣金状态
	 **/
	public void setBrokerageStatus(Long brokerageStatus){
		this.brokerageStatus=brokerageStatus;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}