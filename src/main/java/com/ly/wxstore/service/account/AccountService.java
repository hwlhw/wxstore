/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.service.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ly.wxstore.entity.MarketingAccount;
import com.ly.wxstore.service.MarketingAccountService;
import com.ly.wxstore.service.weixin.WeixinUserInfoService;

/**
 * 用户管理类.
 * 
 * @author calvin
 */
// Spring Service Bean的标识.
@Component
@Transactional
public class AccountService {

	private static Logger logger = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	private WeixinUserInfoService weixinUserInfoService;

	@Autowired
	private MarketingAccountService marketingAccountService;

	public MarketingAccount getByOpenid(String openid) {
		return marketingAccountService.getByOpenid(openid);
	}

	/**
	 * 通过OAuth页面授权注册账号
	 * 
	 * @param openid
	 */
	public MarketingAccount regiest(String openid, String registerFrom) {
		logger.info("通过OAuth页面授权注册账号");
		return marketingAccountService.register(openid, registerFrom);
	}

	/**
	 * 根据code读取微信UserInfo
	 * 
	 * @param code
	 * @return Openid
	 */
	public String receiveWeixinUserInfo(String code) {

		return weixinUserInfoService.receiveWeixinUserInfo(code);
	}
}
