package com.ly.wxstore.entity.goods;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class GoodsOrderDetail {

	public GoodsOrderDetail() {
	}

	private Long id; //
	private String orderCode; // 订单编号
	private Long goodsId; // 商品id
	private Double costPrice; // 进货价格
	private Double price; // 销售价格（折前价格）
	private Double realPrice; // 实际销售价格（折后价格）
	private String goodsDesc; // 商品描述
	private String goodsName; // 商品名称
	private Integer goodsNumber; // 购买数量
	private Long cartId; // 购物车id，如果该商品是从购物车下单的，则会有购物车id，用于移除购物车中的商品
	private String goodsImageUrl;

	public String getGoodsImageUrl() {
		return goodsImageUrl;
	}

	public void setGoodsImageUrl(String goodsImageUrl) {
		this.goodsImageUrl = goodsImageUrl;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 订单编号
	 **/
	public String getOrderCode() {
		return orderCode;
	}

	/**
	 * 订单编号
	 **/
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	/**
	 * 商品id
	 **/
	public Long getGoodsId() {
		return goodsId;
	}

	/**
	 * 商品id
	 **/
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	/**
	 * 进货价格
	 **/
	public Double getCostPrice() {
		return costPrice;
	}

	/**
	 * 进货价格
	 **/
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	/**
	 * 销售价格（折前价格）
	 **/
	public Double getPrice() {
		return price;
	}

	/**
	 * 销售价格（折前价格）
	 **/
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 实际销售价格（折后价格）
	 **/
	public Double getRealPrice() {
		return realPrice;
	}

	/**
	 * 实际销售价格（折后价格）
	 **/
	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	/**
	 * 商品描述
	 **/
	public String getGoodsDesc() {
		return goodsDesc;
	}

	/**
	 * 商品描述
	 **/
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	/**
	 * 商品名称
	 **/
	public String getGoodsName() {
		return goodsName;
	}

	/**
	 * 商品名称
	 **/
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	/**
	 * 购买数量
	 **/
	public Integer getGoodsNumber() {
		return goodsNumber;
	}

	/**
	 * 购买数量
	 **/
	public void setGoodsNumber(Integer goodsNumber) {
		this.goodsNumber = goodsNumber;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	private Double total;// 合计 = goodsNumber * realPrice

	public Double getTotal() {
		this.total = (goodsNumber == null ? 0 : goodsNumber) * (realPrice == null ? 0.0 : realPrice);
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

}