package com.ly.wxstore.comm;

import java.util.HashMap;
import java.util.Map;

/**
 * 支付状态: 0：未支付；1：支付成功；2：支付失败
 * 
 * @author Administrator
 *
 */
public class PayStatus {

	private static Map<String, String> status;
	static {
		status = new HashMap<String, String>();
		status.put("0", "未支付");
		status.put("1", "支付成功");
		status.put("2", "支付失败");
	}

	/**
	 * 根据订单支付状态code获取订单支付状态名称
	 * 
	 * @param code
	 * @return String 订单支付状态名称
	 */
	public static String getPayStatusName(String code) {
		return status.get(code);
	}

	/**
	 * 未付款
	 */
	public static final Long UNPAY = 0L;
	/**
	 * 支付成功
	 */
	public static final Long PAY_SUCCESS = 1L;
	/**
	 * 支付失败
	 */
	public static final Long PAY_FAIL = 2L;

}
