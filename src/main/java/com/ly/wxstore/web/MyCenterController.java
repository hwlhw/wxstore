/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.web;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.entity.weixin.WeixinUserInfo;
import com.ly.wxstore.exception.ApiErrorInfo;
import com.ly.wxstore.exception.ServerException;
import com.ly.wxstore.service.MarketingAccountService;
import com.ly.wxstore.service.SalerDownHierarchyService;
import com.ly.wxstore.service.account.ShiroDbRealm.ShiroUser;
import com.ly.wxstore.service.weixin.WeixinConf;
import com.ly.wxstore.service.weixin.WeixinUserInfoService;
import com.ly.wxstore.vo.page.MyCenterPageVo;

/**
 * 管理员管理用户的Controller.
 * 
 * @author peter
 */
@Controller
@RequestMapping(value = "/MyCenter")
public class MyCenterController {

	private static Logger logger = LoggerFactory.getLogger(MyCenterController.class);

	@Autowired
	private WeixinUserInfoService weixinUserInfoService;
	@Autowired
	private SalerDownHierarchyService salerDownHierarchyService;
	@Autowired
	private MarketingAccountService marketingAccountService;

	@Autowired
	private WeixinConf weixinConf;

	@RequiresRoles("user")
	@RequestMapping(value = "userInfolist.json", method = RequestMethod.GET)
	@ResponseBody
	public MyCenterPageVo list() {
		try {
			String openid = getCurrentUser().getOpenid();

			MyCenterPageVo vo = new MyCenterPageVo();

			// 微信用户信息
			WeixinUserInfo weixinUserInfo = weixinUserInfoService.getByOpenIdAndWeixinPublicId(openid, weixinConf.getPublicId());
			vo.setNickname(weixinUserInfo.getNicknameForBase64Decoder());
			vo.setImageUrl(weixinUserInfo.getHeadimgurl());

			return vo;
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}

	/**
	 * 使用Ehcache缓存 ShiroUse 信息，如果缓存失效，user里面的数据将会丢失
	 * 
	 * @return
	 */
	private ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}

	@ExceptionHandler(ServerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApiErrorInfo handleInvalidRequestError(ServerException ex) {
		return new ApiErrorInfo(ex.getCode(), ex.getMessage());
	}

}
