'use strict';

/* Filters */

var AppFilters = angular.module('OutlerCenterApp.filters', []);

AppFilters.filter('orderStatusNameFilter', function () {
    return function (text) {
    	/**
    	 * ----------订单状态----------
			1.待付款
			2.待发货
			3.待收货
			4.已收货
			5.已取消（付款前可以取消）
			6.退款（付款后，确认收货前可以申请退款，因为有可能快递原因导致未收到货）
		 */
    	var statusStr = "未知状态";
    	if(text == 1){
    		statusStr = "待付款";
    	}else if(text == 2){
    		statusStr = "待发货";
    	}else if(text == 3){
    		statusStr = "待收货";
    	}else if(text == 4){
    		statusStr = "已收货";
    	}else if(text == 5){
    		statusStr = "已取消";
    	}else if(text == 6){
    		statusStr = "已退款";
    	}
    	
    	
        return statusStr;
    }
});




