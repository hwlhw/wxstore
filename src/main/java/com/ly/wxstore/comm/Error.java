package com.ly.wxstore.comm;

/**
 * <h4>错误码定义</h4>
 * <p>
 * 五位数字，第一位定义为错误类型识别码。如20002
 * </p>
 * <ul>
 * <li>10 开头的定义为系统内部错误</li>
 * <li>20 开头的定义为安全（认证、授权、权限）相关的错误</li>
 * <li>30 开头的码定义为订单和支付相关错误</li>
 * <li>40 开头的码定义为与腾讯微信服务器交互相关错误</li>
 * </ul>
 * 
 * @author Peter
 *
 */
public class Error {
	// ------------------- 1开头的定义为系统内部错误 -------------------
	/**
	 * 系统繁忙
	 */
	public static final String _10001 = "10001";
	public static final String _10001_MSG = "系统繁忙";

	// ------------------- 2开头的定义为安全（认证、授权、权限）相关的错误 -------------------
	public static final String _20002 = "20002";
	public static final String _20002_MSG = "20002_MSG 登录失败后的回调";

	// -------------------- 3开头的定义为订单和支付相关错误 ----------------------------------
	public static final String _30002 = "30002";
	public static final String _30002_MSG = "没有选择商品";
	
	public static final String _30003 = "_30003";
	public static final String _30003_MSG = "收货地址不存在";
	
	public static final String _30004 = "30004";
	public static final String _30004_MSG = "未选择收货地址";
	
	

	// --------------------- 4 开头的码定义为与腾讯微信服务器交互相关错误 ----------------------
	public static final String _40001 = "40001";
	public static final String _40001_MSG = "读取微信图片资源失败";
	
	public static final String _40002 = "40002";
	public static final String _40002_MSG = "微信支付失败";//调用微信统一下单接口失败
	
	public static final String _40003 = "40003";
	public static final String _40003_MSG = "微信支付失败";//解析微信返回结果失败（XML解析失败）
	
	public static final String _40004 = "40004";
	public static final String _40004_MSG = "微信支付失败";//微信统一下单接口调用失败（入参错误）
	
	public static final String _40005 = "40005";
	public static final String _40005_MSG = "微信支付失败";//调用微信统一下单接口返回数据被攻击
	
	public static final String _40006 = "40006";
	public static final String _40006_MSG = "支付结果通知 - 报文解析错误";//调用微信统一下单接口返回数据被攻击
	
	public static final String _40007 = "40007";
	public static final String _40007_MSG = "支付结果通知 - 网络数据正确性校验错误";//调用微信统一下单接口返回数据被攻击
	
	
	

	// --------------------- 5 开头的码定义为页面参数错误 ----------------------
	public static final String _50001 = "50001";
	public static final String _50001_MSG = "亲最少选择一件商品";
	

}
