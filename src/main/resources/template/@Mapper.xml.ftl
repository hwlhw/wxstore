[#ftl] 
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" 
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!-- namespace必须指向Dao接口 -->
<mapper namespace="com.ly.wxstore.repository.${class.name}Dao">
	<resultMap id="${class.name?uncap_first}Map" type="com.ly.wxstore.entity.${class.name}">
		<id property="id" column="id" />
		[#list class.fields as field] 
		<result property="${field.name}" column="${field.columnName}" />
	    [/#list] 
	</resultMap>
	
	<!-- 根据Id查询 -->
	<select id="getById" parameterType="long" resultMap="${class.name?uncap_first}Map">
		select * from ${class.tableName} where id=${r"#{id}"}
	</select>
	
	<!-- 查询所有 -->
	<select id="getAll" resultMap="${class.name?uncap_first}Map">
		select * from ${class.tableName} 
	</select>

	
	<!-- 查询 -->
	<select id="searchPage" parameterType="map" resultMap="${class.name?uncap_first}Map">
		select * from ${class.tableName} t 
		order by id DESC 
		LIMIT ${r"#{pageStart},#{pageSize}"}
	</select>
	
	<!-- 查询总数 -->
	<select id="searchCount" parameterType="map" resultType="Long">
		select count(1) from  ${class.tableName} t 
	</select>

	<!-- 保存 -->
	<insert id="save" parameterType="com.ly.wxstore.entity.${class.name}" useGeneratedKeys="true" keyProperty="id">
		insert into ${class.tableName} (
			[#list class.fields as field]${field.columnName},[/#list])
		values (
			[#list class.fields as field]${r"#{"}${field.name}${r"}"},[/#list])
	</insert>
	
	<!-- 更新 -->
	<update id="update" parameterType="com.ly.wxstore.entity.${class.name}">
		update ${class.tableName} t set 
			t.id=${r"#{id}"} 
			[#list class.fields as field]
			[#if field.type=="String"]
			<if test="${field.name} != null and ${field.name} != '' ">
			,${field.columnName}=${r"#{"}${field.name}${r"}"}
			</if>
			[#else]
			<if test="${field.name} != null">
			,${field.columnName}=${r"#{"}${field.name}${r"}"}
			</if>
			[/#if]
			[/#list]
		where t.id=${r"#{id}"}
	</update>
	
	<!-- 删除 -->
	<delete id="delete" parameterType="long">
	     update ${class.tableName} set deleted=0 where id=${r"#{id}"}
	</delete>
	

</mapper> 
