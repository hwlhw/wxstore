package com.ly.wxstore.service.goods;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.Delete;
import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.goods.Goods;
import com.ly.wxstore.entity.goods.StoreCart;
import com.ly.wxstore.repository.goods.GoodsDao;
import com.ly.wxstore.repository.goods.StoreCartDao;
import com.ly.wxstore.service.account.ShiroDbRealm.ShiroUser;
import com.ly.wxstore.service.weixin.WeixinConf;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class StoreCartService {

	private static Logger logger = LoggerFactory.getLogger(StoreCartService.class);

	@Autowired
	private StoreCartDao storeCartDao;

	@Autowired
	private GoodsDao goodsDao;

	@Autowired
	private WeixinConf weixinConf;

	public StoreCart getById(Long id) {
		return storeCartDao.getById(id);
	}

	public List<StoreCart> getAll() {
		return storeCartDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<StoreCart> searchPage(StoreCart storeCart, int currentPage, int pageSize) {
		MyPage<StoreCart> myPage = new MyPage<StoreCart>();

		Long count = storeCartDao.searchCount(storeCart);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<StoreCart> list = storeCartDao.searchPage(storeCart, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(StoreCart storeCart) {
		storeCartDao.save(storeCart);
	}

	public void update(StoreCart storeCart) {
		storeCartDao.update(storeCart);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		storeCartDao.delete(id);
	}

	/**
	 * 添加商品到购物车
	 * 
	 * @param goodsId
	 * @param currentUser
	 */
	public void addToCart(Long goodsId, ShiroUser currentUser) {
		StoreCart cartGoods = storeCartDao.getByGoodsId(goodsId,currentUser.getSid());
		// 购物车已有该商品，数量+1
		if (cartGoods != null) {
			cartGoods.setGoodsNumber(cartGoods.getGoodsNumber() + 1);
			storeCartDao.update(cartGoods);
		} else {
			cartGoods = new StoreCart();
			Goods goods = goodsDao.getById(goodsId);
			cartGoods.setCostPrice(goods.getCostPrice());
			cartGoods.setCreateDate(new Date());
			cartGoods.setCreateSid(currentUser.getSid());
			cartGoods.setDeleted(Delete.Enabled.longValue());
			cartGoods.setGoodsDesc(goods.getGoodsDesc());
			cartGoods.setGoodsId(goods.getId());
			cartGoods.setGoodsName(goods.getGoodsName());
			cartGoods.setGoodsNumber(1L);
			cartGoods.setOpenid(currentUser.getOpenid());
			cartGoods.setPrice(goods.getPrice());
			cartGoods.setRealPrice(goods.getRealPrice());
			cartGoods.setWeixinPublicId(weixinConf.getPublicId());
			cartGoods.setToOrder(StoreCart.TO_ORDER_NO);
			storeCartDao.save(cartGoods);
			
		}

	}

	public List<StoreCart> getBySid(String sid) {
		return storeCartDao.getBySid(sid);
	}
}
