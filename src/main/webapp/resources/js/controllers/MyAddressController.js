'use strict';

/**
 * 我的收货地址 MyAddressController
 * MyAddressController
 * @constructor
 */
WxStoreApp.controller('MyAddressController', ["$rootScope","$scope","$http","$ionicPopup","$ionicModal",
	function($rootScope, $scope, $http, $ionicPopup, $ionicModal) {
	$scope.fetchConsigneeAddressList = function() {
		console.log('MyAddressController');
        $http.get('Address/addressList.json').success(function(addressList){
        	$scope.addressList = addressList;
        }).error(function(data, status, headers, config) {
	       	 $rootScope.wxstore.showError(data);
        });
    };
    $scope.fetchConsigneeAddressList();
    
	 //下拉刷新页面
     $scope.doRefresh = function() {
    	 $http.get('Address/addressList.json').success(function(addressList){
             $scope.addressList = addressList;
         }).error(function(data, status, headers, config) {
        	 $rootScope.wxstore.showError(data);
         }).finally(function() {
        	 $scope.$broadcast('scroll.refreshComplete');
         });
	 };
    
	$scope.data = {
	    showDelete: false
	};
	
	$scope.onItemEdit = function(addr) {
		if(addr){
			$scope.addr = addr;
		}
	    $scope.modal.show();
	};
	  
	$scope.onItemDelete = function(addr) {
		console.log(addr);
		$rootScope.wxstore.showLoading();
		$http.delete('Address/delete/'+addr.id+'.json').success(function(){
			$scope.addressList.splice($scope.addressList.indexOf(addr), 1);
			$rootScope.wxstore.hideLoading();
			$scope.modal.hide();
	    }).error(function(data, status, headers, config) {
	    	$rootScope.wxstore.hideLoading();
	    	console.log(data);
	    	$rootScope.wxstore.showError(data);
	    });
		
	};
	
	//初始化CreateAddress Model Window
	$ionicModal.fromTemplateUrl('resources/templates/my-address-create.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

	//打开CreateAddress Model Window
	$scope.onCreate = function(){
		$scope.addr={};//新增页面，设置addr为空对象
	    $scope.modal.show();
	}
	
	//关闭CreateAddress Model Window
	$scope.closeModal = function(){
	    $scope.modal.hide();
	}
	
	//保存地址
	$scope.saveAddress = function(){
		$rootScope.wxstore.showLoading();
		
		console.log($scope.addr);
		$http.post('Address/save.json',$scope.addr).success(function(addr){
			if($scope.addr.id && $scope.addr.id > 0){
				//编辑
			}else{
				//新增
				$scope.addressList.unshift(addr);
			}
			$rootScope.wxstore.hideLoading();
			$scope.modal.hide();
	    }).error(function(data, status, headers, config) {
	    	$rootScope.wxstore.hideLoading();
	    	console.log(data);
	    	$rootScope.wxstore.showError(data);
	    });
	}
 
	 
}]);
