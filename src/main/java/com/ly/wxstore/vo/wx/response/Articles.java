package com.ly.wxstore.vo.wx.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Articles")
@XmlAccessorType(XmlAccessType.FIELD)
public class Articles {
	
	@XmlElement(name="item")
	private List<Article> items = new ArrayList<Article>();

	public List<Article> getItems() {
		return items;
	}

	public void setItems(List<Article> items) {
		this.items = items;
	}

	

	
	
	

}
