package com.ly.wxstore.repository.goods;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.goods.GoodsMarketingStrategy;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface GoodsMarketingStrategyDao {
	
	GoodsMarketingStrategy getByGoodsId(Long goodsId);
	
	List<GoodsMarketingStrategy> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<GoodsMarketingStrategy> searchPage(@Param("goodsMarketingStrategy")GoodsMarketingStrategy goodsMarketingStrategy,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(GoodsMarketingStrategy goodsMarketingStrategy);
	
	void save(GoodsMarketingStrategy goodsMarketingStrategy);
	
	void update(GoodsMarketingStrategy goodsMarketingStrategy);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}
