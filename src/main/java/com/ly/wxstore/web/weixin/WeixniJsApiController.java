/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.web.weixin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ly.wxstore.comm.ResultCode;
import com.ly.wxstore.service.weixin.JsapiSignService;
import com.ly.wxstore.vo.JsapiSignResult;
import com.ly.wxstore.vo.JsonResult;

/**
 * 管理员管理用户的Controller.
 * 
 * @author peter
 */
@Controller
@RequestMapping(value = "/jsapi")
public class WeixniJsApiController {

	private static Logger logger = LoggerFactory.getLogger(WeixniJsApiController.class);

	@Autowired
	private JsapiSignService jsapiSignService;

	@RequestMapping(value = "test", method = RequestMethod.GET)
	public String list(Model model) {
		return "weix-js-api-test/test";
	}

	// jsapi 接入
	@RequestMapping(value = "ajax/sign", method = RequestMethod.POST)
	@ResponseBody
	public JsonResult jsapiSign(@ModelAttribute("url") String url, Model model) {
		logger.info("---------- jsapi 接入 --------url:"+url);
		JsapiSignResult result = new JsapiSignResult();

		try {
			result = jsapiSignService.sign(url);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(ResultCode.ERROR_MSG);
			result.setCode(ResultCode.ERROR);
			logger.error("---------- jsapi 接入   --------", e);
		}
		return result;
	}
	

}
