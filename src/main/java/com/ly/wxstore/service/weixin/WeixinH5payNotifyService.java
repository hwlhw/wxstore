package com.ly.wxstore.service.weixin;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springside.modules.mapper.JaxbMapper;
import org.springside.modules.mapper.JsonMapper;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.comm.OrderStatus;
import com.ly.wxstore.comm.PayStatus;
import com.ly.wxstore.comm.WeixinPaySignUtils;
import com.ly.wxstore.comm.XmlUtils;
import com.ly.wxstore.entity.goods.Goods;
import com.ly.wxstore.entity.goods.GoodsOrder;
import com.ly.wxstore.entity.goods.GoodsOrderDetail;
import com.ly.wxstore.entity.weixin.WeixinH5payNotify;
import com.ly.wxstore.entity.weixin.WeixinPublic;
import com.ly.wxstore.exception.ServerException;
import com.ly.wxstore.repository.goods.GoodsOrderDao;
import com.ly.wxstore.repository.weixin.WeixinH5payNotifyDao;
import com.ly.wxstore.utils.HttpUtil;
import com.ly.wxstore.vo.wx.response.PayNotifyResponse;
import com.ly.wxstore.vo.wx.templatemsg.OrderPaySuccessTemplateMsg;
import com.ly.wxstore.web.weixin.WeixniJsApiController;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinH5payNotifyService {

	private static final String _COLOR_FAIL = "#FF0000";

	private static final String _COLOR_SUCCESS = "#329933";

	private static final String OK = "OK";

	private static final String SUCCESS = "SUCCESS";

	private static Logger logger = LoggerFactory.getLogger(WeixinH5payNotifyService.class);
	
	JsonMapper jsonMap = new JsonMapper();
	
	@Autowired
	private WeixinPublicService weixinPublicService;
	
	@Autowired
	private WeixinAccessTokenService weixinAccessTokenService;

	@Autowired
	private WeixinH5payNotifyDao weixinH5payNotifyDao;
	
	@Autowired
	private GoodsOrderDao goodsOrderDao;

	public WeixinH5payNotify getById(Long id) {
		return weixinH5payNotifyDao.getById(id);
	}

	public List<WeixinH5payNotify> getAll() {
		return weixinH5payNotifyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinH5payNotify> searchPage(WeixinH5payNotify weixinH5payNotify, int currentPage, int pageSize) {
		MyPage<WeixinH5payNotify> myPage = new MyPage<WeixinH5payNotify>();

		Long count = weixinH5payNotifyDao.searchCount(weixinH5payNotify);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinH5payNotify> list = weixinH5payNotifyDao.searchPage(weixinH5payNotify, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinH5payNotify weixinH5payNotify) {
		weixinH5payNotifyDao.save(weixinH5payNotify);
	}

	public void update(WeixinH5payNotify weixinH5payNotify) {
		weixinH5payNotifyDao.update(weixinH5payNotify);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinH5payNotifyDao.delete(id);
	}

	
	/**
	 * 通知频率为15/15/30/180/1800/1800/1800/1800/3600，单位：秒<br/>
	 * 
	 * 由于存在重新发送后台通知的情况，因此同样的通知可能会多次发送给商户系统。商户系统必须能够正确处理重复的通知。
		推荐的做法是，当收到通知进行处理时，首先检查对应业务数据的状态，判断该通知是否已经处理过，
		如果没有处理过再进行处理，如果处理过直接返回结果成功。在对业务数据进行状态检查和处理之前，要采用数据锁进行并发控制，以避免函数重入造成的数据混乱。
	 * @param weixinH5payNotifyXml
	 * @return
	 * @throws ServerException 需要微信服务器再次发出通知
	 */
	public synchronized PayNotifyResponse processPayNotify(String weixinH5payNotifyXml){
		
		PayNotifyResponse resp = new PayNotifyResponse();
		resp.setReturn_code(SUCCESS);
		resp.setReturn_msg(OK);
		
		Map<String, String> resultMap = null;
		try {
			resultMap = XmlUtils.toMap(weixinH5payNotifyXml);
		} catch (DocumentException e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._40006, Error._40006_MSG);
		}
		
		WeixinH5payNotify weixinH5payNotify = JaxbMapper.fromXml(weixinH5payNotifyXml, WeixinH5payNotify.class);
		
		//是否已成功处理过该订单的通知，直接返回，否则执行处理逻辑
		if(isNotified(weixinH5payNotify)){
			return resp;
		}
		
		//网络数据正确性校验，如果失败说明有人恶意修改数据，需要
		//    1. 记录日志，发出报警，人工定位原因，给出业务解决方案；
		//    2. 返回FAIL，告知微信服务器重新发起订单确认
		WeixinPublic weixinPublic = weixinPublicService.getGlobleWeixinPublic();
		String sign = weixinH5payNotify.getSign();
		
		String mySign = WeixinPaySignUtils.payUniSign(resultMap, weixinPublic.getPayApiKey());
		weixinH5payNotify.setMySign(mySign);
		weixinH5payNotify.setCreateDate(new Date());
		//1. 记录日志 保存到数据库
		weixinH5payNotifyDao.save(weixinH5payNotify);
		if (!mySign.equals(sign)) {
			// 2. 返回FAIL，告知微信服务器重新发起订单确认
			logger.error("微信【支付结果通知】接口返回数据被篡改：mySign:" + mySign + ",sign:" + sign);
			throw new ServerException(Error._40007, Error._40007_MSG);
		}
		
		GoodsOrder order = goodsOrderDao.getByOrderCode(weixinH5payNotify.getOutTradeNo());
		OrderPaySuccessTemplateMsg tmsg = new OrderPaySuccessTemplateMsg();
		
		if(SUCCESS.equals(weixinH5payNotify.getReturnCode()) && SUCCESS.equals(weixinH5payNotify.getResultCode())){
			//支付成功，修改订单状态为“待发货”、支付状态为“支付成功”
			goodsOrderDao.updateOrderStatusAndPayStatus(weixinH5payNotify.getOutTradeNo(),OrderStatus.WAITING_DELIVER,PayStatus.PAY_FAIL);
			
			// 发送模板消息 订单支付成功
			tmsg.setTemplate_id("jmmy3GvKqNqoySPzzaLDOMVoWAA3MECxx5dMfwhT3cA");
			tmsg.setTopcolor(_COLOR_SUCCESS);
			tmsg.setTouser(order.getOpenid());
			tmsg.setUrl("http://wx.wonderlink.com.cn/wxstore");
			tmsg.getData().getFirst().setValue("订单支付成功！店小二马上为您发货，欢迎再次购买！订单编号："+order.getOrderCode());
			tmsg.getData().getFirst().setColor(_COLOR_SUCCESS);
			tmsg.getData().getOrderMoneySum().setColor(_COLOR_SUCCESS);
			tmsg.getData().getOrderProductName().setColor(_COLOR_SUCCESS);
			tmsg.getData().getRemark().setColor(_COLOR_SUCCESS);
			tmsg.getData().getRemark().setValue("店小二马上为您发货，欢迎再次购买！");
			
		}else{
			//修改订单支付状态为：2：支付失败
			goodsOrderDao.updatePayStatus(weixinH5payNotify.getOutTradeNo(),PayStatus.PAY_FAIL);

			// 发送模板消息 订单支付失败
			tmsg.setTemplate_id("jmmy3GvKqNqoySPzzaLDOMVoWAA3MECxx5dMfwhT3cA");
			tmsg.setTopcolor(_COLOR_FAIL);
			tmsg.setTouser(order.getOpenid());
			tmsg.setUrl("http://wx.wonderlink.com.cn/wxstore");
			tmsg.getData().getFirst().setValue("抱歉，订单支付失败，请重新下单！订单编号："+order.getOrderCode());
			tmsg.getData().getFirst().setColor(_COLOR_FAIL);
			tmsg.getData().getOrderMoneySum().setColor(_COLOR_FAIL);
			tmsg.getData().getOrderProductName().setColor(_COLOR_FAIL);
			tmsg.getData().getRemark().setColor(_COLOR_FAIL);
			tmsg.getData().getRemark().setValue("抱歉，订单支付失败，请重新下单！");
		}
		
		//订单金额
		tmsg.getData().getOrderMoneySum().setValue(order.getRealMoney()+"元");
		
		//设置商品名称，多个商品用、分割
		String orderProductName = "";
		for (GoodsOrderDetail goods : order.getGoodsList()) {
			orderProductName+=goods.getGoodsName()+"、";
		}
		orderProductName = orderProductName.substring(0, orderProductName.length()-1);
		tmsg.getData().getOrderProductName().setValue(orderProductName);
		
		//发送模板消息【订单支付成功】
		sendTemplateMsg(tmsg);
		
		return resp;
	}

	/**
	 * 发送模板消息【订单支付成功】
	 * @param tmsg
	 */
	private void sendTemplateMsg(OrderPaySuccessTemplateMsg tmsg) {
		String requestJson = jsonMap.toJson(tmsg);
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+weixinAccessTokenService.getAccessToken();

		String responseJson = "";
		try {
			responseJson = HttpUtil.httpPost(url, requestJson);
		} catch (Exception e) {
			logger.error("模板消息发送失败", e);
			//throw new ServerException(Error._40002, Error._40002_MSG);
		}
	}

	/**
	 * 是否已经处理过订单通知
	 * @param weixinH5payNotify
	 * @return
	 */
	private boolean isNotified(WeixinH5payNotify weixinH5payNotify) {
		boolean result = false;
		List<WeixinH5payNotify> list= weixinH5payNotifyDao.getByOrderCode(weixinH5payNotify.getOutTradeNo());
		if(list!=null && list.size()>0){
			for (WeixinH5payNotify notify : list) {
				//如果通知签名一样，说明已经成功接收了订单通知。
				if(notify.getSign().equals(notify.getMySign())){
					result = true;
				}
			}
		}
		
		return result;
	}
}
