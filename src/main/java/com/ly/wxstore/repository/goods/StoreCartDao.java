package com.ly.wxstore.repository.goods;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.goods.StoreCart;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface StoreCartDao {
	
	StoreCart getById(Long id);
	
	List<StoreCart> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<StoreCart> searchPage(@Param("storeCart")StoreCart storeCart,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(StoreCart storeCart);
	
	void save(StoreCart storeCart);
	
	void update(StoreCart storeCart);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	List<StoreCart> getBySid(String sid);

	StoreCart getByGoodsId(@Param("goodsId")Long goodsId,@Param("sid")String sid);
	

}
